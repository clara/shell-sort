"Sort a list of songs based on their number of plays."

import sys

def order_items(songs: list, i: int, gap: int) -> list:
    cancion = songs[i][0]
    reproducciones = songs[i][1]
    j = i
    while j >= gap and songs[j - gap][1] < reproducciones:
        songs[j] = songs[j - gap]
        j -= gap
        songs[j] = (cancion, reproducciones)
    return songs

def sort_music(songs: list) -> list:
    total = len(songs)
    gap = total // 2
    for i in range(gap, total):
        songs = order_items(songs, i, gap)
    gap //= 2
    return songs

def create_dictionary(arguments):
    songs: dict = {}
    pos = 0
    try:
        while pos < len(arguments):
            song_name = arguments[pos]
            pos += 1
            if pos < len(arguments):
                reproducciones = int(arguments[pos])
                songs[song_name] = reproducciones
            else:
                sys.exit(f"Error: '{arguments[pos]}' no es un número válido.")
            pos += 1
    except:
        sys.exit("Error: Se ha de proporcionar un número válido de reproducciones para la canción.")
        return songs

def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0:
        print("Error: Debes proporcionar pares de canción y número de reproducciones.")
        return

    song_data: list = []
    for i in range(0, len(args), 2):
        song_name = args[i]
        num_reproducciones = int(args[i + 1])
        song_data.append((song_name, num_reproducciones))

    song_dict: dict = dict(song_data)

    sorted_songs: list = list(song_dict.items())
    sort_music(sorted_songs)

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()
